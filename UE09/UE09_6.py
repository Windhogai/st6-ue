import plotly.graph_objects as go
import numpy as np
import os

class Data:
    def __init__(self, vCtrl):
        self.vCtrl = vCtrl
        self.iDS = []
        self.vDS = []
    iDS : list = []
    vDS : list = []
    vCtrl : int = 0

def parse_data(directory : str, file_name : str):
    data = []
    with open(directory + "/" + file_name, 'r') as file:
        values = []
        for line in file:
            line = line.strip() 
            if line.startswith('Step Information:'):
                currentLine = line.split('=')[1].split(' ')[0].strip()
                try:
                    vCtrl = float(currentLine)  
                except: 
                    print("[ERROR]: parsing int (" + str(currentLine) + ") failed")
                data.append(Data(vCtrl))
            else:
                try:
                    currentLine = line.split('\t')
                    data[-1].iDS.append(float(currentLine[1]))
                    data[-1].vDS.append(float(currentLine[0]))
                except: 
                    print("[ERROR]: parsing to float (" + str(currentLine) + ") failed")
    return data


directory = "D:/Repositories/st6-ue/UE09"
file_3 = "UE09_3.txt"
file_5 = "UE09_5.txt"
# file_names = os.listdir(directory)

data_3 = parse_data(directory, file_3)
data_5 = parse_data(directory, file_5)

fig = go.Figure()
VGS0_3 = go.Scatter(
     x=data_3[10].vDS,
     y=data_3[10].iDS, 
     name="VGS = 0V",
     mode='lines')
VGS1_3 = go.Scatter(
     x=data_3[8].vDS,
     y=data_3[8].iDS, 
     name="VGS = -1V",
     mode='lines')
VGS0_5 = go.Scatter(
     x=data_5[10].vDS,
     y=data_5[10].iDS, 
     name="VGS = 0V, linear",
     mode='lines')
VGS1_5 = go.Scatter(
     x=data_5[8].vDS,
     y=data_5[8].iDS,
     name="VGS = -1V, linear",
     mode='lines')
VGS0_ideal = go.Scatter(
     x=data_5[10].vDS,
     y=np.asarray(data_5[10].vDS)/46.8759522, 
     name="R=46.48, ideal",
     mode='lines')
VGS1_ideal = go.Scatter(
     x=data_5[8].vDS,
     y=np.asarray(data_5[8].vDS)/62.9145123,
     name="R=62.91, ideal",
     mode='lines')

fig.add_trace(VGS0_3)
fig.add_trace(VGS1_3)
fig.add_trace(VGS0_5)
fig.add_trace(VGS1_5)
fig.add_trace(VGS0_ideal)
fig.add_trace(VGS1_ideal)

layout = go.Layout(
            yaxis=dict(title='IDS in A', title_font=dict(size=18)),
            xaxis=dict(title='VDS in V', title_font=dict(size=18)))
fig.update_layout(layout)
fig.show()


