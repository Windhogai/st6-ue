import numpy as np
import plotly.graph_objects as go
import plotly.express as px
from scipy import signal as sig
from scipy import stats

class resultType:
    def __init__(self, values, name, n):
        self.values = values
        self.name = name
        self.n = n
    values: list
    name: str
    n: int

def movingAverage(signal: list, windowSize : int):
    b = np.full(windowSize, 1/windowSize) # array of length windowSize filled with 1/windowSize
    a = [1]
    return resultType(sig.lfilter(b, a, signal), "ma, m=" + str(windowSize), windowSize)

def exponentialMovingAverage(signal: list, n : int):
    if(n == 0):
        RuntimeError("exponentialMovingAverage: n == 0")
    x = [0]
    for idx in range(1, len(signal)):
        x.append((x[-1]*(n-1) + signal[idx])/n)
    return resultType(x, "ema, n=" + str(n), n)

def calculateStdDeviation(values: list, name: str, originalValues = resultType([], "", 0)):
    if (len(originalValues.values) == 0):
        stdDev = np.round(stats.tstd(values),2)
        print(name + ":\tstdDeviation = " + str(np.round(stdDev,2)))
    else:
        stdDevRef = stats.tstd(originalValues.values)
        stdDev = stats.tstd(values)
        print(name + ":\tstdDeviation = " + str(np.round(stdDev,2)) + ", noise reduction by factor: " + str(np.round(stdDevRef/stdDev,2)))
    return stdDev

def testForNormalDistribution(values: list, name: str, alpha = 0.05):
    #I do everthing to try to reject the null hypthesis but in failing that 
    # i conclude that the data has to be normally distributed since i cannot disprove it
    # null hypthesis: the data set follows a normal distribution
    # alternative hypthesis: the data set does not follow a normal distribution
    # statistical significance level: alpha = 0.05
    # if the p value is smaller than 0.05 i cannot be sure that the date is normally distributed
    # -> i reject the null hypothesis -> i conclude my initial assumption that the data is 
    # normally distributed is false -> it has to be not normally distributed
    # if the p value is greater than 0.05 i cannot disprove my initial assumption that the data
    # is normally distributed -> i conclude that the data is normally distributed
    k2, p = stats.normaltest(values)
    print(name + ":\tp=" + str(np.round(p,2)) + " normally distributed: ", end='')
    if(p < alpha):
        print("NO")
    else:
        print("YES")

#### config #####
exercise1 = False
exercise2 = False
exercise3 = False
exercise4 = False
exercise5 = False
#################

np.random.seed(42)  # Set a seed for reproducibility
# Generate random values with normal distribution
values = np.random.normal(loc=0, scale=1, size=1000)

originalValues = resultType(values, "original", 0)

ma2 = movingAverage(values, 2)
ma4 = movingAverage(values, 4)
ma10 = movingAverage(values, 10)

ema2 = exponentialMovingAverage(values, 2)
ema4 = exponentialMovingAverage(values, 4)
ema10 = exponentialMovingAverage(values, 10)

resultsMA = [ma2, ma4, ma10]
resultsEMA = [ema2, ema4, ema10]
 
if(exercise1):
    trace1 = go.Histogram(x=values, name='original values')
    layout = go.Layout(title='Histogram', barmode='overlay')
    fig = go.Figure(data=[trace1], layout=layout)
    fig.show()

if(exercise2):
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        y=values, 
        mode='markers',
        name="original values"))
    fig.add_trace(go.Scatter(
        y=ma2.values, 
        mode='markers',
        name="MA windowSize 2"))
    fig.add_trace(go.Scatter(
        y=ma4.values, 
        mode='markers',
        name="MA windowSize 4"))
    fig.add_trace(go.Scatter(
        y=ma10.values, 
        mode='markers',
        name="MA windowSize 10"))
    fig.update_layout(
        xaxis_title='samples',
        yaxis_title='amplitude in a.u.',
        title="filtered time series with MA filter")
    fig.show()

if(exercise3):
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        y=values, 
        mode='lines',
        name="original values"))
    fig.add_trace(go.Scatter(
        y=ema2.values,
        mode='lines',
        name="EMA n=2"))
    fig.add_trace(go.Scatter(
        y=ema4.values,
        mode='lines',
        name="EMA n=4"))
    fig.add_trace(go.Scatter(
        y=ema10.values, 
        mode='lines',
        name="EMA n=10"))
    fig.update_layout(
        xaxis_title='samples',
        yaxis_title='amplitude in a.u.',
        title="filtered time series with EMA filter")
    fig.show()
    
if(exercise4):
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        y=ma10.values,
        mode='lines',
        name="MA n=10"))
    fig.add_trace(go.Scatter(
        y=ema10.values,
        mode='lines',
        name="EMA n=10"))
    fig.update_layout(
        xaxis_title='samples',
        yaxis_title='amplitude in a.u.',
        title="Comparision MA vs. EMA (n=10)")
    fig.show()
    
if(exercise5):
    trace1 = go.Histogram(x=ma2.values, name='m=2')
    trace2 = go.Histogram(x=ma4.values, name='m=4')
    trace3 = go.Histogram(x=ma10.values, name='m=10')

    calculateStdDeviation(originalValues.values, originalValues.name)
    calculateStdDeviation(ma2.values, ma2.name, originalValues)
    calculateStdDeviation(ma4.values, ma4.name, originalValues)
    calculateStdDeviation(ma10.values, ma10.name, originalValues)

    # Create layout
    layout = go.Layout(title='Histogram with MA filter')

    # Create figure
    fig = go.Figure(data=[trace1, trace2, trace3], layout=layout)

    # Show the figure
    fig.show()
    
    trace1 = go.Histogram(x=ema2.values, name='n=2')
    trace2 = go.Histogram(x=ema4.values, name='n=4')
    trace3 = go.Histogram(x=ema10.values, name='n=10')
    
    calculateStdDeviation(ema2.values,  ema2.name, originalValues)
    calculateStdDeviation(ema4.values,  ema4.name, originalValues)
    calculateStdDeviation(ema10.values, ema10.name, originalValues)

    # Create layout
    layout = go.Layout(title='Histogram with EMA filter')

    # Create figure
    fig = go.Figure(data=[trace1, trace2, trace3], layout=layout)

    # Show the figure
    fig.show()