import numpy as np
import plotly.graph_objects as go
import plotly.express as px
from scipy import signal as sig
from scipy import stats

class resultType:
    def __init__(self, values, name, n):
        self.values = values
        self.name = name
        self.n = n
    values: list
    name: str
    n: int

def movingAverage(signal: list, windowSize : int):
    b = np.full(windowSize, 1/windowSize) # array of length windowSize filled with 1/windowSize
    a = [1]
    return resultType(sig.lfilter(b, a, signal), "ma, m=" + str(windowSize), windowSize)

def exponentialMovingAverage(signal: list, n : int):
    if(n == 0):
        RuntimeError("exponentialMovingAverage: n == 0")
    x = [0]
    for idx in range(1, len(signal)):
        x.append((x[-1]*(n-1) + signal[idx])/n)
    return resultType(x, "ema, n=" + str(n), n)

#### config #####
exercise7 = True
#################

values = np.append(np.full(10, 0),np.full(50,1))
originalValues = resultType(values, "original", 0)

ma2 = movingAverage(values, 2)
ma4 = movingAverage(values, 4)
ma10 = movingAverage(values, 10)

ema2 = exponentialMovingAverage(values, 2)
ema4 = exponentialMovingAverage(values, 4)
ema10 = exponentialMovingAverage(values, 10)

resultsMA = [ma2, ma4, ma10]
resultsEMA = [ema2, ema4, ema10]
 
if(exercise7):
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        y=values, 
        mode='markers',
        name="original values"))
    fig.add_trace(go.Scatter(
        y=ma2.values, 
        mode='markers',
        name="MA m=2"))
    fig.add_trace(go.Scatter(
        y=ma4.values, 
        mode='markers',
        name="MA m=4"))
    fig.add_trace(go.Scatter(
        y=ma10.values, 
        mode='markers',
        name="MA m=10"))
    fig.update_layout(
        xaxis_title='samples',
        yaxis_title='amplitude in a.u.',
        title="step response with MA filter")
    fig.show()
    
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        y=values, 
        mode='markers',
        name="original values"))
    fig.add_trace(go.Scatter(
        y=ema2.values, 
        mode='markers',
        name="EMA n=2"))
    fig.add_trace(go.Scatter(
        y=ema4.values, 
        mode='markers',
        name="EMA n=4"))
    fig.add_trace(go.Scatter(
        y=ema10.values, 
        mode='markers',
        name="EMA n=10"))
    fig.update_layout(
        xaxis_title='samples',
        yaxis_title='amplitude in a.u.',
        title="step response with EMA filter")
    fig.show()