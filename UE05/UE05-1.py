import plotly.graph_objs as go

# Load data from file
with open('./UE05-3.txt', 'r') as f:
    lines = f.readlines()[1:] # skip header line
    data = [line.strip().split('\t') for line in lines]
    vin = [float(row[1]) for row in data]
    vout = [float(row[2]) for row in data]

# Create plotly figure
fig = go.Figure()
fig.add_trace(go.Scatter(x=vout, y=vin, mode='lines+markers'))

# Set axis labels and title
fig.update_layout(
    xaxis_title='V(vout) in V',
    yaxis_title='V(vin) in V',
    title='V(vin) vs V(vout)'
)

# Show plot in browser
fig.show()
