import plotly.graph_objs as go

# Load data from UE05-1.txt
with open('UE05-1.txt', 'r') as f:
    lines = f.readlines()[1:] # skip header line
    data = [line.strip().split('\t') for line in lines]
    vin1 = [float(row[1]) for row in data]
    vout1 = [float(row[2]) for row in data]

# Load data from UE03-1.txt
with open('UE05-3.txt', 'r') as f:
    lines = f.readlines()[1:] # skip header line
    data = [line.strip().split('\t') for line in lines]
    vin2 = [float(row[1]) for row in data]
    vout2 = [float(row[2]) for row in data]

# Load data from UE03-1.txt
with open('UE05-7.txt', 'r') as f:
    lines = f.readlines()[1:] # skip header line
    data = [line.strip().split('\t') for line in lines]
    vin3 = [float(row[1]) for row in data]
    vout3 = [float(row[2]) for row in data]

# Create plotly figure
fig = go.Figure()
fig.add_trace(go.Scatter(x=vout1, y=vin1, mode='markers+lines', name='UE05-1'))
fig.add_trace(go.Scatter(x=vout2, y=vin2, mode='markers+lines', name='UE05-3'))
fig.add_trace(go.Scatter(x=vout3, y=vin3, mode='markers+lines', name='UE05-7'))

# Set axis labels and title
fig.update_layout(
    xaxis_title='V(vout)',
    yaxis_title='V(vin)',
    title='V(vin) vs V(vout)'
)

# Show plot in browser
fig.show()
